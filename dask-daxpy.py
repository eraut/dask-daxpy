import numpy as np
import dask.distributed
import dask_mpi
import sys
import time

def rand_vec(seed, size):
    return np.random.RandomState(seed).uniform(0.0, 1.0, size)

def daxpy(seed, r, x, y):
    res = np.zeros(x.shape)
    rng = np.random.RandomState(seed)
    for _ in range(r):
        res += (x*rng.uniform(0.0,1.0,x.shape) + y)
    return res

if __name__ == "__main__":

    dask_mpi.initialize(interface='ib0')
    with dask.distributed.Client() as client:

        n = 2
        nb = 1
        r = 1
        # Arguments
        for i in range(len(sys.argv)):
            if sys.argv[i] == '-n':
                n = int(sys.argv[i+1])
            elif sys.argv[i] == '-b':
                nb = int(sys.argv[i+1])
            elif sys.argv[i] == '-r':
                r = int(sys.argv[i+1])
        bs = n//nb

        print('n = ', n)
        print('nb = ', nb)
        print('r = ', r)

        x = [client.submit(rand_vec, i, bs) for i in range(nb)]
        y = [client.submit(rand_vec, nb + i, bs) for i in range(nb)]
        dask.distributed.wait(x)
        dask.distributed.wait(y)

        start = time.time()
        z = [client.submit(daxpy, 2*nb+i, r, x[i], y[i]) for i in range(nb)]

        # Printing
        # print("x =")
        # for i in range(nb):
        #     print(x[i].compute())
        # print("y =")
        # for i in range(nb):
        #     print(y[i].compute())

        #z_res = [z[i].compute() for i in range(nb)]
        #print(z_res[0][0])

        # Wait on results
        z_res = client.gather(z)
        t = time.time() - start
        print("Time: ", t, "s")

        # print("z =")
        # for i in range(nb):
        #     print(z_res[i])
        print("===== Scheduler logs =====")
        for log in client.get_scheduler_logs():
            print(log)
        print("Done!")
