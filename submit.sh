#!/bin/bash

#SBATCH --job-name=dask-daxpy
#SBATCH --ntasks-per-node=1
#SBATCH --nodes=4
#SBATCH --time=30:00
#SBATCH -p short-40core

module load shared
module load anaconda/3

source activate dask-env

module load intel-stack

mpirun python dask-daxpy.py -n 1024 -b 4
